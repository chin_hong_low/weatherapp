import React, { useEffect, useState } from 'react';

import {getNow} from '../common/untils';

const API_KEY = 'b058ea2c2765e46ddd28b30b1fe926e4';

// function addZero(i) {
//   if (i < 10) {
//     i = "0" + i;
//   }
//   return i;
// }

// function getNow() {
//   var d = new Date();
//   var h = addZero(d.getHours());
//   var m = addZero(d.getMinutes());
//   var s = addZero(d.getSeconds());
//   return h + ":" + m + ":" + s;
// }

const WeatherCard = ({item={}, index, onRemove})=>{
  const [city, setCity] = useState(item);

  useEffect(()=>{
    setCity(item);
    if(item.interval && !isNaN(item.interval)){
      const interval = setInterval(() => {
        console.log('This will run every second!');
        fetch(`http://api.openweathermap.org/data/2.5/weather?units=metric&q=${item.name}&appid=${API_KEY}`)
        .then(response => response.json())
        .then((result)=>{
          if(result && result.weather && result.weather.length > 0){
            const weather = result.weather[0] || {};
            const record = {name: result.name, temp: result.main ? result.main.temp : '-', weather: weather.main, icon: weather.icon, interval: item.interval, updated: getNow()};
            setCity(record);
          }
        })
        .catch(error=>{
          alert(error);
        })
      }, item.interval * 1000);

      return () => clearInterval(interval);
    }
  }, [item])

  return (
    <div key={index} className="weather-card">
      <div className="weather-card-title">{city.name}</div>
      <div className="weather-card-close">
        <button onClick={()=>{onRemove(index)}}>X</button>
      </div>
      <div className="weather-card-image">
        <img alt={city.icon} src={`http://openweathermap.org/img/wn/${city.icon}@2x.png`}/>
      </div>
      <div className="weather-card-footer">
        <div>{city.temp} C</div>
        <div>{city.weather}</div>
      </div>
      <div style={{fontSize: '8px', textAlign: 'left', marginTop: '5px'}}>updated: {city.updated}</div>
    </div>
  )
}

export default WeatherCard;