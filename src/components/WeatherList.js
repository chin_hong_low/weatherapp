import React, {useState, useRef} from 'react';
import WeatherCard from './WeatherCard';
import {getNow} from '../common/untils';

const API_KEY = 'b058ea2c2765e46ddd28b30b1fe926e4';


const WeatherList = ()=>{
  const [items, setItems] = useState([]);
  const cityRef = useRef();
  const intervalRef = useRef();
  
  const onEnter = ()=>{
    const intervalSelect = intervalRef.current;
    const cityInput = cityRef.current;
    fetch(`http://api.openweathermap.org/data/2.5/weather?units=metric&q=${cityInput.value}&appid=${API_KEY}`)
    .then(response => response.json())
    .then((result)=>{
      console.log(result);
      if(result.message){
        throw new Error(result.message);
      }
      if(result && result.weather && result.weather.length > 0){
        const weather = result.weather[0] || {};
        const record = {name: result.name, temp: result.main ? result.main.temp : '-', weather: weather.main, icon: weather.icon, interval: intervalSelect.value, updated: getNow()};
        
        let indexFound = items.findIndex(item=>item.name.toLowerCase() === cityInput.value.toLowerCase());
        if(indexFound != null && indexFound >= 0){
          console.log("record : ", record, indexFound);
          setItems(prev=>{
            return prev.map(item=>{
              if(item.name.toLowerCase() === cityInput.value.toLowerCase()){
                return record;
              }else{
                return item
              }
            })
          });
        }else{
          setItems(prev=>[...prev, record]);
        }
      }
    })
    .catch(error=>{
      alert(error.message);
    })
  }

  const onRemove = (index)=>{
    console.log(index);
    setItems(prev=>{let newData = [...prev]; newData.splice(index, 1); return newData;});
  }

  console.log("List : ", items);
  return (
    <div>
      <div className="weather-list">
        {
          items.map((item, index)=>{
            return (
              <WeatherCard key={index} item={item} index={index} onRemove={onRemove} {...item}/>
            )
          })
        }
      </div>
      <div className="weather-form">
        <input name="city" ref={cityRef}/>
        <select name="interval" ref={intervalRef}>
          <option value={10}>10 sec</option>
          <option value={15}>15 sec</option>
          <option value={20}>20 sec</option>
        </select>
        <button onClick={onEnter}>Add</button>
      </div>
    </div>    
  )
}

export default WeatherList;